## AspNetFormsAuth

This repository demonstrates how forms authentication works within an ASP.NET web forms project. The code within loosely goes off of a tutorial at https://docs.microsoft.com/en-us/troubleshoot/aspnet/forms-based-authentication.