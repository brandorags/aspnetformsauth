﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetFormsAuth._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3><asp:Label runat="server" ID="LoggedInMessage"></asp:Label></h3>
    <input type="submit" Value="Sign Out" runat="server" id="CmdSignOut" />
</asp:Content>
