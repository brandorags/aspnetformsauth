﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace AspNetFormsAuth
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var currentUser = HttpContext.Current.User;
            var identity = currentUser?.Identity as FormsIdentity;
            if (identity == null)
            {
                SignOut();
            }
            else if (identity.IsAuthenticated)
            {
                LoggedInMessage.Text = $"You are logged in, {identity.Name}!";
            }
        }

        protected void CmdSignOut_ServerClick(object sender, EventArgs e)
        {
            SignOut();
        }

        private void SignOut()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}