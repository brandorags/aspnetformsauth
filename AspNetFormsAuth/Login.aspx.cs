﻿using System;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using AspNetFormsAuth.Enum;
using AspNetFormsAuth.Helper;
using AspNetFormsAuth.Model;

namespace AspNetFormsAuth
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var currentUser = HttpContext.Current.User;
            var identity = currentUser?.Identity as FormsIdentity;
            if (identity != null)
            {
                Response.Redirect("Default.aspx", true);
            }
        }

        protected async Task CmdLogin_ServerClick()
        {
            AuthUser user;
            var username = txtUserName.Value;
            var password = txtUserPass.Value;
            var authLocation = GetAuthLocation();

            switch (authLocation)
            {
                case AuthLocation.AzureActiveDirectory:
                    user = await AuthenticationHelper.GetUserViaAzureActiveDirectory(username, password);
                    break;
                case AuthLocation.LocalActiveDirectory:
                    user = AuthenticationHelper.GetUserViaOnPremisesActiveDirectory(username, password);
                    break;
                case AuthLocation.Database:
                    user = await AuthenticationHelper.GetUserViaDatabase(username, password);
                    break;
                default:
                    user = null;
                    break;
            }

            // hack to avoid ThreadAbortException errors when calling GetUserViaOnPremisesActiveDirectory
            await Task.Delay(1);

            try
            {
                if (user != null)
                {
                    var issueDate = DateTime.Now;
                    var expirationDate = issueDate.AddMinutes(FormsAuthentication.Timeout.TotalMinutes);
                    var isPersistent = chkPersistCookie.Checked;

                    var ticket = new FormsAuthenticationTicket(1, user.Username, issueDate, expirationDate,
                        isPersistent, user.Role, FormsAuthentication.FormsCookiePath);
                    var encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    var formsAuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    Response.Cookies.Add(formsAuthCookie);
                    Response.Redirect(FormsAuthentication.GetRedirectUrl(user.Username, isPersistent));
                }
                else
                {
                    lblMsg.Text = "The username/password combination is incorrect. Please try again.";
                }
            }
            catch (DirectoryServicesCOMException ex)
            {
                lblMsg.Text = "An error occurred on the server. Please try again.";
                System.Diagnostics.Debug.WriteLine(ex);
            }
            catch (SqlException ex)
            {
                lblMsg.Text = "An error occurred on the server. Please try again.";
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        private AuthLocation GetAuthLocation()
        {
            AuthLocation authLocation;

            if (AzureActiveDirectoryRadio.Checked)
            {
                authLocation = AuthLocation.AzureActiveDirectory;
            }
            else if (LocalActiveDirectoryRadio.Checked)
            {
                authLocation = AuthLocation.LocalActiveDirectory;
            }
            else
            {
                authLocation = AuthLocation.Database;
            }

            return authLocation;
        }
    }
}