﻿namespace AspNetFormsAuth.Model
{
    public class AuthUser
    {
        public string Username { get; set; }
        public string Role { get; set; }
    }
}
