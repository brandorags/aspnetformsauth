﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Async="true" CodeBehind="Login.aspx.cs" Inherits="AspNetFormsAuth.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Login Page</h3>
    <table>
        <tr>
            <td>Username:</td>
            <td><input id="txtUserName" type="text" runat="server" /></td>
            <td><asp:RequiredFieldValidator ControlToValidate="txtUserName" Display="Static" ErrorMessage="*" runat="server" ID="UserName" /></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input id="txtUserPass" type="password" runat="server" /></td>
            <td><asp:RequiredFieldValidator ControlToValidate="txtUserPass" Display="Static" ErrorMessage="*" runat="server" ID="UserPass" /></td>
        </tr>
        <tr>
            <td>Persistent Cookie:</td>
            <td><asp:CheckBox id="chkPersistCookie" runat="server" autopostback="false" /></td>
            <td></td>
        </tr>
        <tr>
            <td>Authentication Location:</td>
            <br />
            <td><asp:RadioButton id="AzureActiveDirectoryRadio" Text="Azure Active Directory" Checked="True" GroupName="AuthLocationRadioGroup" runat="server" /></td>
            <td><asp:RadioButton id="LocalActiveDirectoryRadio" Text="Local Active Directory" GroupName="AuthLocationRadioGroup" runat="server" /></td>
            <td><asp:RadioButton id="DatabaseRadio" Text="Database" GroupName="AuthLocationRadioGroup" runat="server" /></td>
        </tr>
    </table>
    <input type="submit" value="Logon" runat="server" id="CmdLogin" /><p></p>
    <asp:Label id="lblMsg" ForeColor="red" Font-Name="Verdana" Font-Size="10" runat="server" />
</asp:Content>
