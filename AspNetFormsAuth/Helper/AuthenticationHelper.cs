﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Security;
using System.Threading.Tasks;
using AspNetFormsAuth.Model;
using Microsoft.Identity.Client;

namespace AspNetFormsAuth.Helper
{
    public static class AuthenticationHelper
    {
        public static async Task<AuthUser> GetUserViaAzureActiveDirectory(string username, string password)
        {
            AuthUser user = null;

            var clientId = ConfigurationManager.AppSettings["AzureActiveDirectoryClientId"];
            var authority = ConfigurationManager.AppSettings["AzureActiveDirectoryAuthorityUrl"];
            var scopes = new [] { "user.read" };
            var app = PublicClientApplicationBuilder.Create(clientId).WithAuthority(authority).Build();

            try
            {
                var securePassword = new SecureString();
                foreach (var character in password)
                {
                    securePassword.AppendChar(character);
                }

                var result = await app.AcquireTokenByUsernamePassword(scopes, username, securePassword).ExecuteAsync();
                if (result != null)
                {
                    user = new AuthUser
                    {
                        Username = result.Account.Username,
                        Role = result.Account.Environment
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return user;
        }

        public static AuthUser GetUserViaOnPremisesActiveDirectory(string username, string password)
        {
            AuthUser user = null;

            var adPath = ConfigurationManager.AppSettings["ActiveDirectoryPath"];
            var adDomain = ConfigurationManager.AppSettings["ActiveDirectoryDomain"];
            var domainAndUsername = adDomain + username;
            var entry = new DirectoryEntry(adPath, domainAndUsername, password);

            try
            {
                // this obj value may never get used, but assigning entry.NativeObject
                // forces authentication on the entry object
                var obj = entry.NativeObject;

                var search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");

                var result = search.FindOne();
                if (result != null)
                {
                    user = new AuthUser
                    {
                        Username = result.Properties["cn"][0].ToString(),
                        Role = "still-need-to-figure-this-out"
                    };
                }
            }
            catch (DirectoryServicesCOMException e)
            {
                // no need to throw an exception if the credentials are incorrect
                if (e.Message.Trim() != "The user name or password is incorrect.")
                {
                    throw;
                }
            }

            return user;
        }

        public static async Task<AuthUser> GetUserViaDatabase(string username, string password)
        {
            AuthUser user = null;

            var connectionString = ConfigurationManager.ConnectionStrings["AspNetFormsAuthDb"].ConnectionString;
            var conn = new SqlConnection(connectionString);
            conn.Open();

            // create SqlCommand to select Password column from AuthUser table given supplied username
            var cmd = new SqlCommand("SELECT Username, Role FROM AuthUser WHERE Username=@username AND Password=@password", conn);
            cmd.Parameters.Add("@username", SqlDbType.VarChar, 25);
            cmd.Parameters["@username"].Value = username;
            cmd.Parameters.Add("@password", SqlDbType.VarChar, 25);
            cmd.Parameters["@password"].Value = password;

            // execute command and fetch the user's data
            var uname = "";
            var role = "";
            var reader = await cmd.ExecuteReaderAsync();
            while (reader.Read())
            {
                uname = reader.GetString(0);
                role = reader.GetString(1);
                break;
            }

            // cleanup command and connection objects.
            cmd.Dispose();
            conn.Dispose();

            if (!string.IsNullOrEmpty(uname))
            {
                user = new AuthUser { Username = uname, Role = role };
            }

            return user;
        }
    }
}