﻿namespace AspNetFormsAuth.Enum
{
    public enum AuthLocation
    {
        AzureActiveDirectory,
        LocalActiveDirectory,
        Database
    }
}